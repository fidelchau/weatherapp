package com.gnac.chauk.weatherapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.MenuItem
import com.gnac.chauk.weatherapp.adapters.DayAdapter
import com.gnac.chauk.weatherapp.models.Objects
import com.gnac.chauk.weatherapp.utils.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONObject

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, ResponseListener {

    override val responseContext: Context = this
    private val REQUEST_CHECK_SETTINGS: Int = 22
    lateinit var mAdapter: Adapter<*>

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)


        imageView.setImageResource(when (Persist.using(this).isForestLayout) {
            true -> R.drawable.forest_sunny
            else -> R.drawable.sea_sunnypng
        })

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        forcastRecyclerView.setHasFixedSize(true)
        forcastRecyclerView.layoutManager = layoutManager

        forcastLayout.setOnRefreshListener { refreshWeatherForecast() }


    }

    override fun onResume() {
        super.onResume()
        refreshWeatherForecast()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> {
                if (resultCode == Activity.RESULT_OK)
                    refreshWeatherForecast()
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        refreshWeatherForecast()
        when (item.itemId) {


            R.id.nav_forest -> {
                Persist.using(this)
                        .isForestLayout = true
            }
            R.id.nav_sea -> {
                Persist.using(this)
                        .isForestLayout = false
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    /**
     * Calls the Openweather API for latest weather and weather forcast
     */
    @SuppressLint("MissingPermission")
    private fun refreshWeatherForecast() {

        val locationRequest = LocationRequest.create()?.apply {
            interval = 100000
            fastestInterval = 50000
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest!!)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())//Check if GPS and WIFI are enabled

        task.addOnSuccessListener { locationSettingsResponse ->
            Permissions.check(this, Manifest.permission.ACCESS_COARSE_LOCATION,// Check if app has permission
                    "We need to find out where you are so we can give you the relevant weather",
                    object : PermissionHandler() {
                        override fun onGranted() {


                            fusedLocationClient.lastLocation
                                    .addOnSuccessListener { location: Location? ->
                                        forcastLayout.isRefreshing = true
                                        ServerEndpoints.with(this@HomeActivity)
                                                .getForcast(location)
                                        ServerEndpoints.with(this@HomeActivity)
                                                .getWeather(location)
                                    }
                        }
                    })
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                            this@HomeActivity,
                            REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }


    }

    /**
     * Sets UI elements for today's weather
     */
    fun setTemperature(currentWeatherResponse: Objects.CurrentWeatherResponse) {

        weatherTextView.text = currentWeatherResponse.weather[0].description
        temperatureTextView.text = currentWeatherResponse.main.temp.toDegrees()
        minTempTextView.text = currentWeatherResponse.main.tempMin.toDegrees()
        currentTempTextView.text = currentWeatherResponse.main.temp.toDegrees()
        maxTempTextView.text = currentWeatherResponse.main.tempMax.toDegrees()

        if (Persist.using(this).isForestLayout) {
            imageView.setImageResource(
                    when (currentWeatherResponse.weather[0].main.toLowerCase()) {
                        "clear" -> R.drawable.forest_sunny
                        "clouds" -> R.drawable.forest_cloudy
                        "rain" -> R.drawable.forest_rainy
                        else -> R.drawable.forest_sunny
                    })
            forcastRecyclerView.setBackgroundResource(
                    when (currentWeatherResponse.weather[0].main.toLowerCase()) {
                        "clear" -> R.color.sunny
                        "clouds" -> R.color.cloudy
                        "rain" -> R.color.rainy
                        else -> R.color.sunny
                    })
            mainLayout.setBackgroundResource(
                    when (currentWeatherResponse.weather[0].main.toLowerCase()) {
                        "clear" -> R.color.sunny
                        "clouds" -> R.color.cloudy
                        "rain" -> R.color.rainy
                        else -> R.color.sunny
                    })
        } else {
            imageView.setImageResource(
                    when (currentWeatherResponse.weather[0].main.toLowerCase()) {
                        "clear" -> R.drawable.sea_sunnypng
                        "clouds" -> R.drawable.sea_cloudy
                        "rain" -> R.drawable.sea_rainy
                        else -> R.drawable.sea_sunnypng
                    })
            forcastRecyclerView.setBackgroundResource(
                    when (currentWeatherResponse.weather[0].main.toLowerCase()) {
                        "clear" -> R.color.ocean_blue
                        "clouds" -> R.color.cloudy
                        "rain" -> R.color.rainy
                        else -> R.color.ocean_blue
                    })
            mainLayout.setBackgroundResource(
                    when (currentWeatherResponse.weather[0].main.toLowerCase()) {
                        "clear" -> R.color.ocean_blue
                        "clouds" -> R.color.cloudy
                        "rain" -> R.color.rainy
                        else -> R.color.ocean_blue
                    })
        }


    }


    override fun onResponse(requestCode: Int, jsonObject: JSONObject) {
        val gson = Gson()
        when (requestCode) {
            ServerEndpoints.WEATHER_REQUEST -> {
                val currentWeatherResponse = gson.fromJson<Objects.CurrentWeatherResponse>(
                        jsonObject.toString(),
                        Objects.CurrentWeatherResponse::class.java
                )

                setTemperature(currentWeatherResponse)
            }
            ServerEndpoints.FORCAST_REQUEST -> {
                val forcastResponse = gson
                        .fromJson<Objects.ForcastResponse>(
                                jsonObject.toString(),
                                Objects.ForcastResponse::class.java
                        )

                mAdapter = DayAdapter(forcastResponse.list.filter { it.dtTxt.contains("12:00") })//filter to remain with one reading per day at 12000
                forcastRecyclerView.adapter = mAdapter
                forcastLayout.isRefreshing = false

            }

        }
    }


    override fun onError(requestCode: Int, message: String) {
        forcastLayout.isRefreshing = false
        message.errorAlert(this)
                .setPositiveButton("OK", null)
                .show()
    }
}
