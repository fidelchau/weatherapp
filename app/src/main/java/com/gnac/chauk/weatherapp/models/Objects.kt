package com.gnac.chauk.weatherapp.models

import com.google.gson.annotations.SerializedName

class Objects {
    data class CurrentWeatherResponse(
            @SerializedName("base")
            val base: String,
            @SerializedName("clouds")
            val clouds: Clouds,
            @SerializedName("cod")
            val cod: Int,
            @SerializedName("coord")
            val coord: Coord,
            @SerializedName("dt")
            val dt: Int,
            @SerializedName("id")
            val id: Int,
            @SerializedName("main")
            val main: Main,
            @SerializedName("name")
            val name: String,
            @SerializedName("sys")
            val sys: Sys,
            @SerializedName("weather")
            val weather: List<Weather>,
            @SerializedName("wind")
            val wind: Wind
    )

    data class Main(
            @SerializedName("grnd_level")
            val grndLevel: Double,
            @SerializedName("humidity")
            val humidity: Int,
            @SerializedName("pressure")
            val pressure: Double,
            @SerializedName("sea_level")
            val seaLevel: Double,
            @SerializedName("temp")
            val temp: Double,
            @SerializedName("temp_max")
            val tempMax: Double,
            @SerializedName("temp_min")
            val tempMin: Double
    )

    data class Coord(
            @SerializedName("lat")
            val lat: Double,
            @SerializedName("lon")
            val lon: Double
    )

    data class Sys(
            @SerializedName("country")
            val country: String,
            @SerializedName("message")
            val message: Double,
            @SerializedName("sunrise")
            val sunrise: Int,
            @SerializedName("sunset")
            val sunset: Int
    )


    data class Clouds(
            @SerializedName("all")
            val all: Int
    )


    data class ForcastResponse(
            @SerializedName("city")
            val city: City,
            @SerializedName("cnt")
            val cnt: Int,
            @SerializedName("cod")
            val cod: String,
            @SerializedName("list")
            val list: List<DayForcast>,
            @SerializedName("message")
            val message: Double
    )

    data class City(
            @SerializedName("coord")
            val coord: Coord,
            @SerializedName("country")
            val country: String,
            @SerializedName("id")
            val id: Int,
            @SerializedName("name")
            val name: String
    )


    data class DayForcast(
            @SerializedName("clouds")
            val clouds: Clouds,
            @SerializedName("dt")
            val dt: Int,
            @SerializedName("dt_txt")
            val dtTxt: String,
            @SerializedName("main")
            val main: Main,
            @SerializedName("rain")
            val rain: Rain,
            @SerializedName("sys")
            val sys: Sys,
            @SerializedName("weather")
            val weather: List<Weather>,
            @SerializedName("wind")
            val wind: Wind
    )



    data class Weather(
            @SerializedName("description")
            val description: String,
            @SerializedName("icon")
            val icon: String,
            @SerializedName("id")
            val id: Int,
            @SerializedName("main")
            val main: String
    )


    data class Wind(
            @SerializedName("deg")
            val deg: Double,
            @SerializedName("speed")
            val speed: Double
    )

    class Rain(
    )
}