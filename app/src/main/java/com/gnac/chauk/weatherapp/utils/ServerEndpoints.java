package com.gnac.chauk.weatherapp.utils;

import android.app.ProgressDialog;
import android.location.Location;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gnac.chauk.weatherapp.BuildConfig;

import com.gnac.chauk.weatherapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by fidelity on 2017/05/23.
 * A utility class useed to make API requests
 */

public class ServerEndpoints {

    private static final String BASE_URL = "https://api.openweathermap.org/data/2.5";
    private static final String WEATHER = "/weather";
    private static final String FORCAST = "/forecast";

    public static final int WEATHER_REQUEST = 10;
    public static final int FORCAST_REQUEST = 11;

    private ProgressDialog progressDialog;

    private static ResponseListener responseListener;
    private static RequestQueue queue;
    private static ServerEndpoints INSTANCE;


    /**
     * Constructor
     *
     * @param responseListener
     */
    private ServerEndpoints(ResponseListener responseListener) {
        ServerEndpoints.responseListener = responseListener;
        if (queue == null) queue = Volley.newRequestQueue(responseListener.getResponseContext());


    }

    /**
     * Static access to the Server endpoints
     *
     * @param responseListener
     * @return
     */
    public static ServerEndpoints with(ResponseListener responseListener) {
        if (ServerEndpoints.responseListener == null || !ServerEndpoints.responseListener.getClass().equals(responseListener.getClass())) {
            INSTANCE = new ServerEndpoints(responseListener);

        }
        return INSTANCE;
    }

    private HashMap<String, String> getParamsFromLocation(Location location) {
        HashMap<String, String> params = new HashMap<>();
        params.put("lat", String.valueOf(location.getLatitude()));
        params.put("lon", String.valueOf(location.getLongitude()));
        params.put("units", "metric");
        params.put("APPID", responseListener.getResponseContext().getString(R.string.open_weather_api_key));
        return params;
    }

    /**
     * Creates string to append to url adding the APPID as well
     *
     * @param location
     * @return
     */
    private String buildQuery(Location location) {
        return "?lat=" + location.getLatitude() + "&lon=" + location.getLongitude() +
                "&units=metric&APPID=" + responseListener.getResponseContext().getString(R.string.open_weather_api_key);
    }

    /**
     * Calls API for today's weather
     *
     * @param location
     */
    public void getWeather(Location location) {
        makeTransparentVolleyRequest(WEATHER_REQUEST, BASE_URL + WEATHER + buildQuery(location), getParamsFromLocation(location));
    }

    /**
     * Fetches 5 day forecast
     *
     * @param location
     */
    public void getForcast(Location location) {
        makeTransparentVolleyRequest(FORCAST_REQUEST, BASE_URL + FORCAST + buildQuery(location), getParamsFromLocation(location));
    }

    /**
     * Makes a call to an API using volley
     *
     * @param requestCode
     * @param url
     * @param params
     */
    private void makeVolleyRequest(final int requestCode, final String url, final HashMap<String, String> params) {
        progressDialog = new ProgressDialog(responseListener.getResponseContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {
            log(url);
            log(response);

            try {
                log("Response: " + response);
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.has("fail"))
                    responseListener.onResponse(requestCode, jsonObject);
                else responseListener.onError(requestCode, jsonObject.getString("fail"));
            } catch (JSONException e) {
                e.printStackTrace();
                responseListener.onError(requestCode, "Could not be found");
            }
            progressDialog.dismiss();
        }, error -> {

            log(error.getMessage());

            responseListener.onError(requestCode, "Please make sure you have an internet connection.");


            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                responseListener.onError(requestCode, "Please make sure you have an internet connection.");
            } else if (error instanceof AuthFailureError) {
                //TODO
            } else if (error instanceof ServerError) {
                responseListener.onError(requestCode, "Oops, something went wrong, please try again or contact the administrator.");
            } else if (error instanceof NetworkError) {
                responseListener.onError(requestCode, "Please make sure you have an internet connection.");

            } else if (error instanceof ParseError) {
                responseListener.onError(requestCode, "Oops, something went wrong, please try again or contact the administrator.");
            }
            progressDialog.dismiss();
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };

        log("Request: " + url);
        log("Params: " + params);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 7,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);

    }


    /**
     * Makes a call to an API using volley without showing a progressdialog
     *
     * @param requestCode
     * @param url
     * @param params
     */
    private void makeTransparentVolleyRequest(final int requestCode, final String url, final HashMap<String, String> params) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {


            try {
                log("Response: " + response);
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.has("fail"))
                    responseListener.onResponse(requestCode, jsonObject);
                else responseListener.onError(requestCode, jsonObject.getString("fail"));
            } catch (JSONException e) {
                e.printStackTrace();
                responseListener.onError(requestCode, "Could not be found");
            }
        }, error -> {

            log(error.getMessage());

            responseListener.onError(requestCode, "Please make sure you have an internet connection.");


            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                responseListener.onError(requestCode, "Please make sure you have an internet connection.");
            } else if (error instanceof AuthFailureError) {
                //TODO
            } else if (error instanceof ServerError) {
                responseListener.onError(requestCode, "Oops, something went wrong, please try again or contact the administrator.");
            } else if (error instanceof NetworkError) {
                responseListener.onError(requestCode, "Please make sure you have an internet connection.");

            } else if (error instanceof ParseError) {
                responseListener.onError(requestCode, "Oops, something went wrong, please try again or contact the administrator.");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };

        log("Request: " + url);
        log("Params: " + params);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

    /**
     * Makes synchronous call
     *
     * @param url
     * @param params
     * @return
     */
    private JSONObject makeVolleyFutureRequest(final String url, final HashMap<String, String> params) {
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, future, future) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        queue.add(stringRequest);


        try {
            String result = future.get();
            log("Future returned result: " + result);
            return new JSONObject(result);
        } catch (JSONException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    /**
     * log for debbuging purposes
     *
     * @param message
     */
    private void log(String message) {
        if (BuildConfig.DEBUG)
            Log.i("Weather", "ServerEndpoints: " + message);
    }
}
