package com.gnac.chauk.weatherapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * A utility class to store information in sharedPreferences.
 */
public class Persist {
    private Context context;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    /**
     * Constructor
     *
     * @param context
     */
    private Persist(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * A handle for static access to the persist methods.
     *
     * @param context
     * @return
     */
    public static Persist using(Context context) {
        return new Persist(context);
    }

    /**
     * Internal method to save ints into sharedpreferences
     *
     * @param key   The key
     * @param value The int to be saved
     */
    private void saveInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Method to retrieve saved int from sharedpreferences
     *
     * @param key          Key
     * @param emptyDefault the default
     * @return
     */
    private int rememberInt(String key, int emptyDefault) {

        return sharedPreferences.getInt(key, emptyDefault);
    }

    /**
     * Method to retrieve saved int from sharedpreferences
     *
     * @param key Key
     * @return
     */
    private int rememeberInt(String key) {

        return rememberInt(key, 0);
    }

    /**
     * Internal method to save boolean into sharedpreferences
     *
     * @param key   The key
     * @param value The boolean to be saved
     */
    private void saveBoolean(String key, boolean value) {

        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Method to retrieve saved boolean from sharedpreferences
     *
     * @param key          Key
     * @param emptyDefault the default
     * @return
     */
    private boolean rememberBoolean(String key, boolean emptyDefault) {

        return sharedPreferences.getBoolean(key, emptyDefault);
    }

    /**
     * Method to retrieve boolean int from sharedpreferences
     *
     * @param key Key
     * @return
     */
    private boolean rememberBoolean(String key) {
        return rememberBoolean(key, false);
    }

    /**
     * Internal method to save String into sharedpreferences
     *
     * @param key   The key
     * @param value The String to be saved
     */
    private void saveString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Method to retrieve saved String from sharedpreferences
     *
     * @param key          Key
     * @param emptyDefault the default
     * @return
     */
    private String rememberString(String key, String emptyDefault) {

        return sharedPreferences.getString(key, emptyDefault);
    }

    /**
     * Method to retrieve saved String from sharedpreferences
     *
     * @param key Key
     * @return
     */
    private String rememberString(String key) {
        return rememberString(key, "");
    }

    /**
     * To save the layout theme
     *
     * @param isForestLayout true if the forest theme is to be used, otherwise use the sea theme
     */
    public void setForestLayout(boolean isForestLayout) {
        saveBoolean("isForestLayout", isForestLayout);
    }

    /**
     * To check what theme to use
     *
     * @return True if the forest theme is to be used, otherwise use the sea theme
     */
    public boolean isForestLayout() {
        return rememberBoolean("isForestLayout", true);
    }
}
