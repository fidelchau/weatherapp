package com.gnac.chauk.weatherapp.utils

import android.content.Context
import android.support.v7.app.AlertDialog
import com.gnac.chauk.weatherapp.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * Creates a AlertDialog.Builder from a string
 * @param title the title of the AlertDialog
 * @param context the context to be used to build the dialog
 * @return AlertDialog.Builder
 */
fun String.errorAlert(title:String , context: Context): AlertDialog.Builder{
    return AlertDialog
            .Builder(context, R.style.AlertDialogTheme)
            .setTitle(title)
            .setMessage(this)

}

/**
 * Creates an error AlertDialog.Builder from a string
 * @param context the context to be used to build the dialog
 * @return AlertDialog.Builder
 */
fun String.errorAlert(context: Context): AlertDialog.Builder{
    return this.errorAlert("Error",context)
}

/**
 * Extracts day of the week from date following the format yyyy-MM-dd HH:mm:ss
 * @return day of the week
 */
fun String.getDay():String{
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val date= simpleDateFormat.parse(this)
    val dayDateFormat = SimpleDateFormat("EEEE", Locale.getDefault())
    return dayDateFormat.format(date)
}

/**
 * Casts double to int and appends the degree symbol
 * @return returns temperature as a whole number
 */
fun Double.toDegrees():String{
    return  this.toInt().toString()+"\u00B0"
}