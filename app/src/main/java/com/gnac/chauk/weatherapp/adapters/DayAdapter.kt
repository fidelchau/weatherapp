package com.gnac.chauk.weatherapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gnac.chauk.weatherapp.R
import com.gnac.chauk.weatherapp.models.Objects
import com.gnac.chauk.weatherapp.utils.getDay
import com.gnac.chauk.weatherapp.utils.toDegrees
import kotlinx.android.synthetic.main.row_day_weather_item.view.*

class DayAdapter(val dayForcasts:List<Objects.DayForcast>) : RecyclerView.Adapter<DayAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_day_weather_item, parent, false))
    }

    override fun getItemCount(): Int {
        return dayForcasts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dayForcast = dayForcasts[position]
        holder.dayTextView.text = dayForcast.dtTxt.getDay()
        holder.temperatureTextView.text = dayForcast.main.temp.toDegrees()

        holder.imageView.setImageResource(
            when(dayForcast.weather[0].main.toLowerCase()){
            "clear"->R.drawable.clear
            "clouds"->R.drawable.partlysunny
            "rain"->R.drawable.rain
            else-> R.drawable.clear
        })


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dayTextView = itemView.dayTextView
        val temperatureTextView = itemView.temperatureTextView
        val imageView = itemView.imageView
    }
}