package com.gnac.chauk.weatherapp.utils

import android.content.Context

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by fidelity on 2017/08/28.
 */

interface ResponseListener {

    val responseContext: Context


    /**
     * Callback method for when response is available
     * @param requestCode The code for what api call was made
     * @param jsonObject the response
     *
     */
    @Throws(JSONException::class)
    fun onResponse(requestCode: Int, jsonObject: JSONObject)

    /**
     *Callback method to pass feedback when there is an error.
     * @param requestCode The code for what api call was made
     * @param message The error message
     */
    fun onError(requestCode: Int, message: String)
}
